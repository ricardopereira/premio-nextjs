import { query } from '../../lib/db';

export default async function handler(req, res) {
    console.log('Received request for /api/assignments');
    try {
        const { rows } = await query('SELECT * FROM assignments');
        console.log('Database query executed successfully:', rows);
        res.json(rows);
    } catch (err) {
        console.error('Database query failed:', err);
        res.status(500).json({ message: `Server error: ${err.message}` });
    }
}
