// pages/api/unassign/[number].js
import { query } from '../../../lib/db';

export default async function handler(req, res) {
    const { number } = req.query; // 'number' is extracted from the URL
    console.log(`Attempting to delete assignment for number: ${number}`);

    try {
        const result = await query('DELETE FROM assignments WHERE number = $1', [number]);
        console.log('Delete result:', result);

        if (result.rowCount > 0) {
            console.log(`Assignment for number ${number} deleted successfully.`);
            res.json({ message: 'Atribuição eliminada com sucesso.' });
        } else {
            console.log(`Assignment for number ${number} not found.`);
            res.status(404).json({ message: 'Número não encontrado.' });
        }
    } catch (err) {
        console.error('Error during delete operation:', err);
        res.status(500).json({ message: `Server error: ${err.message}` });
    }
}
