import { query } from '../../lib/db';

export default async function handler(req, res) {
    console.log('Attempting to delete all assignments.');

    try {
        const result = await query('DELETE FROM assignments');
        console.log('Delete all result:', result);

        if (result.rowCount > 0) {
            console.log(`All assignments deleted successfully. Rows affected: ${result.rowCount}`);
            res.json({ message: 'Todas as atribuições foram eliminadas.' });
        } else {
            console.log('No assignments were found to delete.');
            res.json({ message: 'Não havia atribuições para eliminar.' });
        }
    } catch (err) {
        console.error('Error during delete all operation:', err);
        res.status(500).json({ message: `Server error: ${err.message}` });
    }
}
