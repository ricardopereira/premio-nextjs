import { query } from '../../lib/db';

export default async function handler(req, res) {
    const { name, number } = req.body;
    console.log('Attempting to assign number:', number, 'to name:', name);

    try {
        const result = await query('INSERT INTO assignments (number, name) VALUES ($1, $2)', [number, name]);
        console.log('Assignment successful. Result:', result);
        res.json({ message: `Número ${number} adicionada com sucesso a ${name}.` });
    } catch (err) {
        console.error('Error during assignment:', err);
        res.status(500).json({ message: `Server error: ${err.message}` });
    }
}
