import { useState, useEffect } from 'react';

export default function Home() {
    const [assignments, setAssignments] = useState([]);
    const [unassignedNumbers, setUnassignedNumbers] = useState([]);
    const [name, setName] = useState('');
    const [number, setNumber] = useState('');
    const [isLoading, setIsLoading] = useState(true);

    // Define the fetchAssignments function
    const fetchAssignments = () => {
        fetch('/api/assignments')
            .then(response => response.json())
            .then(data => {
                setAssignments(data);
                // Calculate unassigned numbers
                const totalNumbers = 50;
                const assignedNumbers = data.map(assignment => assignment.number);
                const unassigned = [];
                for (let i = 1; i <= totalNumbers; i++) {
                    if (!assignedNumbers.includes(i)) {
                        unassigned.push(i);
                    }
                }
                setUnassignedNumbers(unassigned);
                setIsLoading(false);
            })
            .catch(error => {
                console.error('Failed to fetch assignments:', error);
                setIsLoading(false);
            });
    };

    // Call fetchAssignments on component mount and after state updates
    useEffect(() => {
        setIsLoading(true); // Set loading to true when starting to fetch
        fetchAssignments();
    }, []);

    const handleAssign = (event) => {
        event.preventDefault();
        fetch('/api/assign', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name, number }),
        })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Número já foi atribuído.');
                }
            })
            .then(data => {
                alert('Número atribuído com sucesso.');
                // Reset form fields
                setName('');
                setNumber('');
                // Refresh the list of assignments
                fetchAssignments();
            })
            .catch((error) => {
                // Handle both JSON and text errors
                alert(typeof error === 'string' ? error : error.message);
            });
    };

    const handleReset = () => {
        if (confirm('Tem certeza que deseja limpar todas as atribuições existentes?')) {
            fetch('/api/reset', { method: 'POST' })
                .then(response => response.json())
                .then(data => {
                    alert('Lista de atribuições limpa com sucesso.');
                    // Refresh the list
                    fetchAssignments();
                })
                .catch((error) => {
                    // Handle both JSON and text errors
                    alert(typeof error === 'string' ? error : error.message);
                });
        }
    };

    const handleDelete = (assignment) => {
        if (confirm(`Tem certeza que deseja eliminar ${assignment.number} - ${assignment.name}?`)) {
            fetch(`/api/unassign/${assignment.number}`, { method: 'DELETE' })
                .then(response => {
                    const contentType = response.headers.get('Content-Type');
                    if (response.ok) {
                        if (contentType && contentType.includes('application/json')) {
                            return response.json();
                        } else {
                            throw new Error('Response was not JSON');
                        }
                    } else {
                        if (contentType && contentType.includes('application/json')) {
                            return response.json().then(json => Promise.reject(json));
                        } else {
                            // Non-JSON error, like a 405 HTML error page
                            return response.text().then(text => Promise.reject(text));
                        }
                    }
                })
                .then(data => {
                    alert(data.message); // Display the message from the server
                    fetchAssignments(); // Refresh the list
                })
                .catch((error) => {
                    // Handle both JSON and text errors
                    alert(typeof error === 'string' ? error : error.message);
                });
        }
    };

    if (isLoading) {
        return <div>A carregar os dados...</div>;
    }

    return (
        <div>
            <h1>Atribuir um Número Euromilhões a uma Pessoa</h1>
            <form id="assignment-form" onSubmit={handleAssign}>
                <label htmlFor="name">Nome:</label>
                <input
                    type="text"
                    id="name"
                    name="name"
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <br />
                <label htmlFor="number">Número (1-50):</label>
                <input
                    type="number"
                    id="number"
                    name="number"
                    min="1"
                    max="50"
                    required
                    value={number}
                    onChange={(e) => setNumber(e.target.value)}
                />
                <button type="submit">Atribuir</button>
            </form>

            <h2>Números por atribuir:</h2>
            <p>{unassignedNumbers.length > 0 ? unassignedNumbers.join(', ') : 'Todos os números foram atribuídos.'}</p>

            <h2>Números Atribuídos</h2>
            <ul>
                {assignments.length === 0 ? (
                    <li>De momento não foram atribuídos números.</li>
                ) : (
                    assignments.map((assignment) => (
                        <li key={assignment.number}>
                            Número {assignment.number}: {assignment.name}{' '}
                            <button onClick={() => handleDelete(assignment)}>Eliminar</button>
                        </li>
                    ))
                )}
            </ul>

            <div>
                <p>
                    ⚠️ Apenas usar esta opção quando o sorteio terminar:{' '}
                    <button id="reset-assignments" onClick={handleReset}>
                        Limpar lista de atribuições
                    </button>
                </p>
            </div>
        </div>
    );
}